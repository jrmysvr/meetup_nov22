# School of AI Zurich

**Beginners Meetup - November 22nd, 2019**


[Support Vector Machines notebook](svm_beginners_meetup_iris.ipynb)

[Random Seed notebook](random_seed.ipynb)
